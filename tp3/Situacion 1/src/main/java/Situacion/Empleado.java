package Situacion;

public class Empleado{
    private String nombre;
    private String apellido;
    private double costoHoraDeTrabajo;

    private Turno turnos[];
    private final static int MAXIMA_CANTIDAD_TURNOS=30;
    private int numeroDeTurno;


    private double sueldoNeto;


    //--------constructor-------

    public Empleado(String nombre, String apellido, double costoHoraDeTrabajo){
        this.nombre=nombre;
        this.apellido=apellido;
        this.costoHoraDeTrabajo=costoHoraDeTrabajo;
        turnos = new Turno[MAXIMA_CANTIDAD_TURNOS];
    }


    //---------setters---------
    public void setNombre(String nombre){
        this.nombre= nombre;
    }
    public void setApellido(String apellido){
        this.apellido=apellido;
    }
    public void setCostoHoraDeTrabajo(double costoHoraDeTrabajo){
        this.costoHoraDeTrabajo=costoHoraDeTrabajo;
    }

    //----------getters----------
    public String getNombre(){
        return nombre;
    }
    public String getApellido(){
        return apellido;
    }
    public double getCotoHoraDeTrabajo(){
        return costoHoraDeTrabajo;
    }
    public Turno[] getTurnos(){
        return turnos;
    }
    public double getSueldoNeto(){
        return sueldoNeto;
    }



    public void agreagarTurnos(Turno turno){
        turnos[numeroDeTurno++]=turno;
    }


    
    public long obtenerHorasTrabajadas(){
        long horasTrabajadas=0;
        for (Turno turno : turnos){
            if(turno != null){
                long horaTurno = turno.calcularHorasTrabajadas();
                horasTrabajadas= horasTrabajadas + horaTurno;
            }else {
                break;
            }
        }
        return horasTrabajadas;
    }

    

    public double calcularSueloPorHorasTrabajadas(){
       double sueldoNeto=0;
       sueldoNeto= costoHoraDeTrabajo * obtenerHorasTrabajadas();
       return sueldoNeto;
    }


    
    public  void mostrarInformacion(){
        System.out.println("Empleado :  Apellido y Nombre=" + getApellido() + " "+ getNombre());
        System.out.println("SueldoNeto = " + calcularSueloPorHorasTrabajadas());  // se recorre en obtenerHorasTrabajadas()
    }


}