package Situacion;

import java.time.Duration;
import java.time.LocalDateTime;

public class Turno {
    private LocalDateTime fechaHoraDeIngreso;
    private LocalDateTime fechaHoraDeEgreso;
    private Empleado empleados[];

    private final static int MAXIMA_CANTIDAD_EMPLEADOS = 20;

    private int numeroEmpleado;

    // -------constructor--------

    public Turno(LocalDateTime fechaHoraDeIngreso, LocalDateTime fechaHoraDeEgreso) {
        this.fechaHoraDeIngreso = fechaHoraDeIngreso;
        this.fechaHoraDeEgreso = fechaHoraDeEgreso;
        empleados = new Empleado[MAXIMA_CANTIDAD_EMPLEADOS];
    }

    // --------Setters-------

    public void setFechaHoraDeIngreso(LocalDateTime fechaHoraDeIngreso) {
        this.fechaHoraDeIngreso = fechaHoraDeIngreso;
    }

    public void setFechaHoraDeEgreso(LocalDateTime fechaHoraDeEgreso) {
        this.fechaHoraDeEgreso = fechaHoraDeEgreso;
    }

    // -------getters-------

    public LocalDateTime getFechaHoraDeIngreso() {
        return fechaHoraDeIngreso;
    }

    public LocalDateTime getFechaHoraDeEgreso() {
        return fechaHoraDeEgreso;
    }

    public Empleado[] getEmpleados(){
        return empleados;
    }


    public void agregarEmpleados(Empleado empleado) {   // no se si corresponde hacer esta relación
        empleados[numeroEmpleado++] = empleado;         // asociación de muchos(1..*) a muchos(1..*)
    }


    public long calcularHorasTrabajadas(){//LocalDateTime fechaHoraIngreso, LocalDateTime fechaHoraEgreso){
        long diferenciaDeFechaHora= Duration.between(fechaHoraDeIngreso,fechaHoraDeEgreso).toHours();
        return diferenciaDeFechaHora;
    }


}