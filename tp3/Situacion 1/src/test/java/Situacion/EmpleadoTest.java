package Situacion;

import static org.junit.Assert.assertEquals;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import org.junit.Test;

public class EmpleadoTest {

    @Test
    public void calcularHorasTrabajadas(){
        Empleado empleado = new Empleado("Sergio", "Tula", 400.00);

        DateTimeFormatter formatoFecha = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");

        Turno turno = new Turno(LocalDateTime.parse("24/09/2020 06:00:00", formatoFecha), LocalDateTime.parse("24/09/2020 14:00:00",formatoFecha));
        Turno turno1 = new Turno(LocalDateTime.parse("25/09/2020 22:00:00", formatoFecha), LocalDateTime.parse("26/09/2020 06:00:00",formatoFecha));


        Turno turnos[] = new Turno[30];
        turnos[0]= turno;
        turnos[1]= turno1;

        empleado.agreagarTurnos(turno);
        empleado.agreagarTurnos(turno1);

        long horasTrabajadas = empleado.obtenerHorasTrabajadas();

        long horasDeTrabajoEsperadas=16;

        assertEquals(horasDeTrabajoEsperadas, horasTrabajadas);
    }

    @Test
    public void testCalcularSueldoPorHorasTrabajadas(){
        Empleado empleado = new Empleado("Sergio", "Tula", 300.58);
        DateTimeFormatter formatoFecha = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
        Turno turno = new Turno(LocalDateTime.parse("24/09/2020 06:00:00", formatoFecha), LocalDateTime.parse("24/09/2020 14:00:00",formatoFecha));
        Turno turno1 = new Turno(LocalDateTime.parse("25/09/2020 22:00:00", formatoFecha), LocalDateTime.parse("26/09/2020 06:00:00",formatoFecha));

        Turno turnos[] = new Turno[30];
        turnos[0]= turno;
        turnos[1]= turno1;

        empleado.agreagarTurnos(turno);
        empleado.agreagarTurnos(turno1);

        double sueldoNetoAcutal = empleado.calcularSueloPorHorasTrabajadas();
        double sueldoNetoEsperado = 4809.28;   // 300.58 * 16 hs

        assertEquals(sueldoNetoEsperado, sueldoNetoAcutal, 0.1);

    }

}
