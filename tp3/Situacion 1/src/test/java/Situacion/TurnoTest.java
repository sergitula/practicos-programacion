package Situacion;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.junit.Test;

public class TurnoTest {

    @Test
    public void testCalularHorasTrabajadas(){
        DateTimeFormatter formatoFecha = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
        Turno turno = new Turno(LocalDateTime.parse("24/09/2020 06:00:00", formatoFecha), LocalDateTime.parse("24/09/2020 14:00:00",formatoFecha));


        long diferenciaFechaHoraActual= turno.calcularHorasTrabajadas();
        long diferenciaFechaHoraEsperada= 8;

        assertEquals(diferenciaFechaHoraEsperada, diferenciaFechaHoraActual);
    }
    

    //se agrega correctamente la persona al array
    @Test public void testAgregarEmpleadoAlTurno(){

        DateTimeFormatter formatoFecha = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
        Turno turno = new Turno(LocalDateTime.parse("24/09/2020 06:00:00", formatoFecha), LocalDateTime.parse("24/09/2020 14:00:00",formatoFecha));

        Empleado empleado = new Empleado("Juan", "Castro", 600.00);

        turno.agregarEmpleados(empleado);

        Empleado empleadoEnElTurnoAcutal[]= turno.getEmpleados();

        Empleado empleadoEnElTurnoEsperado []= new Empleado[20];

        empleadoEnElTurnoEsperado[0]= empleado;

        assertArrayEquals(empleadoEnElTurnoEsperado, empleadoEnElTurnoAcutal);


    }


}
