package Situacion;

public class Bebida {

    private double precio;
    private String descripcion;

    private Orden orden;

    // ------Constructor-------

    public Bebida(double precio, String descripcion) {
        this.setPrecio(precio);
        this.setDescripcion(descripcion);
    }

    // -------Setters--------

    public void setOrden(Orden orden) {
        this.orden = orden;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }


    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    //-------Getters--------

    public double getPrecio() {
        return precio;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public Orden getOrden() {
        return orden;
    }


    
}