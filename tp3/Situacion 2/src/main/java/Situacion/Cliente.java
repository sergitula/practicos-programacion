package Situacion;

public class Cliente {
    private String nombre;
    private String apellido;
    private Orden ordenes[];
    private final int MAXIMA_CANTIDAD_ORDENES= 2;  // puede pedir muchas ordenes dependiendo del horario; puede ser mañana o noche
    private int numeroDeOrden;                     


    //-------Constructor-------
    public Cliente(String nombre, String apellido){
        this.nombre= nombre;
        this.apellido= apellido;
        this.ordenes = new Orden[MAXIMA_CANTIDAD_ORDENES];
    }

    //-------Getteres y Setters------
    public String getNombre() {
        return nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Orden[] getOrdenes(){
        return ordenes;
    }


    public void agregarOrdenes(Orden orden){
        ordenes[numeroDeOrden++]= orden;
    }

}
