package Situacion;

public class Efectivo extends Pago{
    private double precioDeOrden;
    private double pagoRecibido;


    //------Constructor------

    public Efectivo(double precioDeOrden, double pagoRecibido){
        super(precioDeOrden, pagoRecibido);
    }
    

    public Boolean pagar(){
        Boolean pagoRealizado;
        if (precioDeOrden== pagoRecibido){
            pagoRealizado= true;
            System.out.println("Pago Realizado.");
        }else{
            pagoRealizado= false;
            System.out.println("Pago NO Realizado."); 
        }
        return pagoRealizado;
    }


}
