package Situacion;

import java.time.LocalTime;
import java.util.ArrayList;

public class Orden {
    private int numeroMesa;
    private int cantidadComensales;
    private String nombreMozo;
    private LocalTime horaInicio;
    private LocalTime horaCierre;

    private Cliente cliente; // Una orden puede tener un unico cliente.
    private Pago pago; // una orden tiene un unico pago.
    private ArrayList<Plato> platos;
    private ArrayList<Bebida> bebidas;

    // Constructor
    public Orden(int numeroMesa, int cantidadComensales, String nombreMozo, LocalTime horaInicio,
            LocalTime horaCierre) {
        this.numeroMesa = numeroMesa;
        this.cantidadComensales = cantidadComensales;
        this.nombreMozo = nombreMozo;
        this.horaInicio = horaInicio;
        this.horaCierre = horaCierre;

        setPlatos(new ArrayList<Plato>());
        setBebidas(new ArrayList<Bebida>());

    }

 
    // setters
    public void setNumeroMesa(int numeroMesa){
        this.numeroMesa= numeroMesa;
    }
    public void setcantidadComensales(int cantidadComensales){
        this.cantidadComensales=cantidadComensales;
    }
    public void setNombreMozo(String nombreMozo){
        this.nombreMozo=nombreMozo;
    }
    public void setHoraInicio(LocalTime horaInicio){
        this.horaInicio=horaInicio;
    }
    public void setHoraCierre(LocalTime horaCierre){
        this.horaCierre=horaCierre;
    }
    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }


    public void setPago(Pago pago) {
        this.pago = pago;
    }

    public void setBebidas(ArrayList<Bebida> bebidas){
        this.bebidas= bebidas;
    }
    public void setPlatos(ArrayList<Plato> platos){
        this.platos = platos;
    }


    //getters
    public int getNumeroMesa(){
        return numeroMesa;
    }
    public int getCantidadComensales(){
        return cantidadComensales;
    }
    public String getNombreMozo(){
        return nombreMozo;
    }
    public LocalTime getHoraInicio(){
        return horaInicio;
    }
    public LocalTime getHoraCierre(){
        return horaCierre;
    }
    public Cliente getCliente() {
        return cliente;
    }


    
    public Pago getPago() {
        return pago;
    }

    public ArrayList<Bebida> getBebidas(){
        return bebidas;
    }
    public ArrayList<Plato> getPlatos(){
        return platos;
    }

    //------Agregar Platos y Bebidas a la orden-------- 
    public void agregarBebida(Bebida bebida){
        this.bebidas.add(bebida);
    }
    public void agregarPlatos(Plato plato){
        this.platos.add(plato);
    }




    public double calcularPrecioTotal(){
        double precioTotalDePlatos =0;  //inicializando variables.
        double precioTotalDeBebidas= 0;

        for (Plato plato: platos ){
            precioTotalDePlatos = plato.getPrecio();
        }

        for (Bebida bebida: bebidas){
            precioTotalDeBebidas = bebida.getPrecio();
        }

        double precioTotal= precioTotalDePlatos +precioTotalDeBebidas ;

        return precioTotal;
    }

}
