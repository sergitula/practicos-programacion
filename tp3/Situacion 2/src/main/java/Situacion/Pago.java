package Situacion;

public abstract class Pago {
    public abstract Boolean pagar();


    private double precioDeOrden;
    private double pagoRecibido;

    private Orden orden; // un pago puede hacerse a una orden.

    // ----constructor-----

    public Pago(double precioDeOrden, double pagoRecibido) {
        this.setPrecioDeOrden(precioDeOrden);
        this.setPagoRecibido(pagoRecibido);
    }


    public double getPagoRecibido() {
        return pagoRecibido;
    }

    public void setPagoRecibido(double pagoRecibido) {
        this.pagoRecibido = pagoRecibido;
    }

    public double getPrecioDeOrden() {
        return precioDeOrden;
    }

    public void setPrecioDeOrden(double precioDeOrden) {
        this.precioDeOrden = precioDeOrden;
    }

    public Orden getOrden() {
        return orden;
    }

    public void setOrden(Orden orden) {
        this.orden = orden;
    }


}
