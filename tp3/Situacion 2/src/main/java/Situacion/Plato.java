package Situacion;

public class Plato {

    private String detallePlato;
    private String detallePostre;
    private double precio;

    private Orden orden;

    // ------Constuctor----------
    public Plato(String detallePlato, String detallePostre, double precio) {
        this.detallePlato = detallePlato;
        this.detallePostre = detallePostre;
        this.precio = precio;
    }


    // ------Getters y Setters-------
    public String getDetallePlato() {
        return detallePlato;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public String getDetallePostre() {
        return detallePostre;
    }

    public void setDetallePostre(String detallePostre) {
        this.detallePostre = detallePostre;
    }

    public void setDetallePlato(String detallePlato) {
        this.detallePlato = detallePlato;
    }


    
    public Orden getOrden() {
        return orden;
    }
    public void setOrden(Orden orden) {
        this.orden = orden;
    }


}