package Situacion;

public class Tarjeta extends Pago {
    // private String titular;
    private Integer porcentajeRecargo;

    private Cliente titular; // El cliente puede ser el titular.

    // -------Constructor--------
    public Tarjeta(double precioDeOrden, double pagoRecibido, Integer porcentajeRecargo) {
        super(precioDeOrden, pagoRecibido);
        this.setPorcentajeRecargo(porcentajeRecargo);

    }

    public Cliente getTitular() {
        return titular;
    }

    public void setTitular(Cliente titular) {
        this.titular = titular;
    }

    public Integer getPorcentajeRecargo() {
        return porcentajeRecargo;
    }

    public void setPorcentajeRecargo(Integer porcentajeRecargo) {
        this.porcentajeRecargo = porcentajeRecargo;
    }

    public Boolean pagar() {
        boolean pagoRealizado;
        double precioConRecargo= super.getPrecioDeOrden() * (porcentajeRecargo/100);
        if (precioConRecargo == super.getPagoRecibido()){
            pagoRealizado= true;
        }else{
            pagoRealizado= false;
        }
        return pagoRealizado;
    }

    
}
