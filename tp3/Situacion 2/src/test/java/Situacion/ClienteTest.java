package Situacion;

import static org.junit.Assert.assertArrayEquals;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import org.junit.Test;
//import static org.junit.Assert.*;

public class ClienteTest {
    @Test public void agregarOrdenACliente(){
        Cliente cliente = new Cliente("Pedro", "Paez");
        DateTimeFormatter formatoHora = DateTimeFormatter.ofPattern("HH:mm:ss");
        Orden orden = new Orden(1, 5, "Pepe", LocalTime.parse("21:00:00",formatoHora), LocalTime.parse("23:00:00",formatoHora));

        cliente.agregarOrdenes(orden);

        Orden ordenActualDelCliente []= cliente.getOrdenes();
        Orden ordenEsperdaDelClietne [] = new Orden [2];

        ordenEsperdaDelClietne[0]= orden;

        assertArrayEquals(ordenEsperdaDelClietne, ordenActualDelCliente);
    }
}
