package Situacion;

import static org.junit.Assert.assertEquals;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import org.junit.Test;

public class OrdenTest {

    @Test
    public void agregarPlatosBebidasOrden(){
        DateTimeFormatter formatoHora = DateTimeFormatter.ofPattern("HH:mm:ss");
        Orden orden = new Orden(1, 5, "Pepe", LocalTime.parse("21:00:00",formatoHora), LocalTime.parse("23:00:00",formatoHora));
        Plato plato = new Plato("Sopa", "helado", 700);
        orden.agregarPlatos(plato);

        Bebida bebida = new Bebida(300, "Vino Blanco");
        Bebida bebida1 = new Bebida(100, "Coca Cola");

        orden.agregarBebida(bebida);
        orden.agregarBebida(bebida1);

        assertEquals(1, orden.getPlatos().size());
        assertEquals(2, orden.getBebidas().size());

   }


    @Test
    public void testCalcularPrecioTotal(){
        DateTimeFormatter formatoHora = DateTimeFormatter.ofPattern("HH:mm:ss");
        Orden orden = new Orden(1, 5, "Pepe", LocalTime.parse("21:00:00",formatoHora), LocalTime.parse("23:00:00",formatoHora));
        Plato plato = new Plato("Sopa", "helado", 700);
        Bebida bebida = new Bebida(300, "Vino Blanco");

        orden.agregarPlatos(plato);
        orden.agregarBebida(bebida);

        double precioActual = orden.calcularPrecioTotal();
        double precioEsperado = 1000;


        assertEquals(precioEsperado, precioActual,0.1);
    }
}
