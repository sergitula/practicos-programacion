package Situacion1;

public class Cliente {
//public class Cliente implements Comparable, Comparator{
    private String apellido;
    private Integer documento;
    private String asunto;
    private String correo;
    private Integer telefono;

    private Domicilio domicilio;

    // ---Constructor---

    public Cliente(String apellido, Integer documento, String asunto, String correo, Integer telefono, Domicilio domicilio) {
        this.apellido = apellido;
        this.documento = documento;
        this.asunto = asunto;
        this.correo = correo;
        this.telefono = telefono;
        this.domicilio = domicilio;
    }




    // ---Setters---
    public void setApellido(String apellido){
        this.apellido= apellido;
    }
    public void setDocumento(Integer documento){
        this.documento= documento;
    }
    public void setAsunto(String asunto){
        this.asunto= asunto;
    }
    public void setCorreo(String correo){
        this.correo= correo;
    }
    public void setTelefono(Integer telefono){
        this.telefono= telefono;
    }

    
    public void setDomicilio(Domicilio domicilio) {
        this.domicilio = domicilio;
    }
    //---gettets---
    public String getApellido(){
        return apellido;
    }
    public Integer getDocumento(){
        return documento;
    }
    public String getAsunto(){
        return asunto;
    }
    public String getCorreo(){
        return correo;
    }
    public Integer getTelefono(){
        return telefono;
    }

    public Domicilio getDomicilio() {
        return domicilio;
    }

    
//    
//    public void agregoDomicilio(Domicilio domicilio){
//        this.domicilio= domicilio;
//    }


    @Override
    public String toString() {
        return "[Cliente --> apellido=" + apellido + ", asunto=" + asunto + ", correo=" + correo + ", documento="
                + documento + ", telefono=" + telefono + ", " + domicilio + "]" + "\n";
    }


//
//    @Override
//    public int compareTo(Object obj) {
//        Cliente client = (Cliente)obj;
//        return this.documento.compareTo(client.getDocumento());
//    }
//
//    @Override
//    public int compare(Object cli1, Object cli2) {
//        Cliente cliente1 = (Cliente) cli1;
//        Cliente cliente2 = (Cliente) cli2;
//        return cliente1.getDocumento().compareTo(cliente2.getDocumento());  // retorna 1, 0, -1 / mayor, igual, menor y ordena segun eso
//
//    }


}