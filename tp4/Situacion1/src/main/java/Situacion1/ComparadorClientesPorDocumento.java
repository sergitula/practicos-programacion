/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Situacion1;

import java.util.Comparator;

/**
 *
 * @author Sergio
 */
public class ComparadorClientesPorDocumento implements Comparator<Cliente>{
    // @Override
    // public int compare(Object cli1, Object cli2) {
    //     Cliente cli1 = (Cliente) cli1;
    //     Cliente cli2 = (Cliente) cli2;
    //     return cli1.getDocumento().compareTo(cli2.getDocumento);  // retorna 1, 0, -1 / mayor, igual, menor y ordena segun eso
    // }

    @Override
    public int compare(Cliente cliente1, Cliente cliente2) {
        return cliente1.getDocumento().compareTo(cliente2.getDocumento());
    }
}
