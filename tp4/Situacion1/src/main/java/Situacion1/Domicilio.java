package Situacion1;

public class Domicilio {
    private Integer numeroCasa;
    private DomicilioSimple simple;
    private DomicilioEdificio edificio;
    private DomicilioBarrial barrio;

    //---Constructor---
    public Domicilio(Integer numeroCasa){
        this.numeroCasa= numeroCasa;
    }

    //--Getters y Setters--
    public Integer getNumeroCasa() {
        return numeroCasa;
    }

    public void setNumeroCasa(Integer numeroCasa) {
        this.numeroCasa = numeroCasa;
    }

    public void verificarDomiclioNulo() throws ClienteSinDomicilioException {
        if(simple != null){
        }else{
            if(edificio != null){
            }else{
                if(barrio != null){
                }else{
                    throw new ClienteSinDomicilioException();
                }
            }
        }
    }


    public String mostrarInformacion() throws ClienteSinDomicilioException{// DomicilioSimple simple,DomicilioEdificio // edificio, DomicilioBarrial barrio){
        verificarDomiclioNulo();

        if (simple != null){
            return this.simple.toString();
        }else{
            if(edificio != null){
                return edificio.toString();
            }else{
                if(barrio != null){
                    return barrio.toString();
                }else{
                    return "Error";
                }
            }           
        }
    }

    

}
