package Situacion1;

public class DomicilioBarrial extends Domicilio {
    private String nombreBarrio;
    private Integer manzana;

    public DomicilioBarrial(Integer numeroCasa,String nombreBarrio, Integer manzana){
        super(numeroCasa);
        this.nombreBarrio=nombreBarrio;
        this.manzana= manzana;
    }

    public String getNombreBarrio() {
        return nombreBarrio;
    }

    public Integer getManzana() {
        return manzana;
    }

    public void setManzana(Integer manzana) {
        this.manzana = manzana;
    }

    public void setNombreBarrio(String nombreBarrio) {
        this.nombreBarrio = nombreBarrio;
    }

    @Override
    public String toString() {
        return "DomicilioBarrial [manzana=" + manzana + ", nombreBarrio=" + nombreBarrio +", numeroCasa: "+ super.getNumeroCasa()+ "]";
    }

    
}
