package Situacion1;

public class DomicilioSimple extends Domicilio {
    private String calle;

    public DomicilioSimple(Integer numeroCasa, String calle){
        super(numeroCasa);
        this.calle= calle;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    @Override
    public String toString() {
        return "DomicilioSimple [calle=" + calle + "]";
    }

    
}
