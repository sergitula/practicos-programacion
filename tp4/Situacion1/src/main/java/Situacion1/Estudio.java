package Situacion1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Estudio  {
    private String nombreEstudio;

    List<Cliente> clientes;

    public Estudio(String nombreEstudio) {
        this.nombreEstudio= nombreEstudio;
        clientes = new ArrayList<Cliente>();
    }


    //---Getters y Setters---
    public String getNombreEstudio() {
        return nombreEstudio;
    }

    public void setNombreEstudio(String nombreEstudio) {
        this.nombreEstudio = nombreEstudio;
    }

    public List<Cliente> getClientes(){
        return clientes;
    } 

    public void setClientes(List <Cliente> cliente){
        this.clientes = cliente;
    }


    public void agregarCliente(Cliente cliente) throws ClienteSinDomicilioException {
        for(Cliente var : clientes){
            if(var.getDocumento().equals(cliente.getDocumento())){
                throw new ClienteExistenteException();
            }
        }
        comprobarDomicilio(cliente.getDomicilio());
        this.clientes.add(cliente);
    }




    public void comprobarDomicilio(Domicilio domicilio) throws ClienteSinDomicilioException {
        domicilio.verificarDomiclioNulo();
    }
 

    public int cantidadClientes(){
        return clientes.size();
    }

    public void getInformacionOrdenadaNumeroDeDocumento(){
        Collections.sort(clientes,new ComparadorClientesPorDocumento());
        mostrarInformacionClientes();
    }

    public void gerInformacionOrdenadaApellido(){
        Collections.sort(clientes, new ComparadorClientesPorApellido());
        mostrarInformacionClientes();
    }

    public void mostrarInformacionClientes(){
        for(Cliente cliente: clientes){
            System.out.println(cliente);
        }
    }
    


    // @Override
    // public String toString() {
    //     return "[" + clientes + "]"+"\n";
    // }


    
}
