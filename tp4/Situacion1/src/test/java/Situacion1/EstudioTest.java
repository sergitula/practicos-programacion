package Situacion1;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class EstudioTest {
    @Test
    public void testComprobarListadoClientesNuevo(){
        Estudio estudio= new Estudio("Estudio Legal");
        assertEquals(0,estudio.cantidadClientes());
    }

    @Test
    public void testAgregarCliente() throws ClienteSinDomicilioException {
        Estudio estudio= new Estudio("Estudio Legal");
        DomicilioEdificio domicilio= new DomicilioEdificio(700, "Pje 1", 3, "prob1");
        Cliente cliente= new Cliente("Paez", 41349234, "Tramitar documentacion", "probando1@gmail.com", 15408734, domicilio);
        
        estudio.agregarCliente(cliente);
        assertEquals(1, estudio.cantidadClientes());
    }

    @Test (expected = ClienteExistenteException.class)
    public void testAgregarClienteExistente() throws ClienteSinDomicilioException {
        Estudio estudio= new Estudio("Estudio Legal");
        DomicilioSimple domSimple= new DomicilioSimple(1345, "invento1df");
        Cliente cliente= new Cliente("Paez", 41349234, "Tramitar documentacion", "probando1@gmail.com", 15408734, domSimple);
        Cliente cliente2= new Cliente("Paez", 41349234, "Tramitar", "probando1@gmail.com", 15408734, domSimple);
        estudio.agregarCliente(cliente);
        estudio.agregarCliente(cliente2);
    }
    
    

    @Test (expected = ClienteSinDomicilioException.class)
    public void testAgregarClienteSinDomicilio() throws ClienteSinDomicilioException {
        Estudio estudio= new Estudio("Estudio Legal");
        Domicilio domicilio = new Domicilio(231);
        Cliente cliente= new Cliente("Paez", 41349234, "Tramitar documentacion", "probando1@gmail.com", 15408734,domicilio);

        estudio.agregarCliente(cliente);
    }

}
