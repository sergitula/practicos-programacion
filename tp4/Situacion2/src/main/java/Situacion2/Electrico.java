package Situacion2;

import java.math.BigDecimal;

public class Electrico extends Herramienta {
    private BigDecimal consumo;

    //constructor
    public Electrico(String codigo, String nombre, BigDecimal precio, BigDecimal consumo) {
        super(codigo, nombre, precio);
    }

    //setters y getters
    public BigDecimal getConsumo() {
        return consumo;
    }

    public void setConsumo(BigDecimal consumo) {
        this.consumo = consumo;
    }

    @Override
    public String getDescripcion() {
        return "Codigo: "+ super.getCodigo() +" , nombre: "+ super.getNombre() + ", precio: " + super.getPrecio() + ", consumo: "+ consumo;
    }

    @Override
    public BigDecimal getPrecioTotal() {
        return super.getPrecio();
    }

    @Override
    public String getCodigo(){
        return super.getCodigo();
    }
}
