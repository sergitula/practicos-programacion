package Situacion2;

import java.math.BigDecimal;
// import java.util.ArrayList;

public class Fabricado extends Material implements ItemVenta {
    private Integer cantidad;
    // private ArrayList <Material> items;

    //---Constructor---
    public Fabricado(String codigo, String nombre, BigDecimal precio, Integer cantidad) {
        super(codigo, nombre, precio);
        this.cantidad= cantidad;
    }

    // ---setters y getters---
    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    // @Override
    // public ArrayList<Material> getItems() {
    //     if(items == null){
    //         items = new ArrayList<>();
    //     }
    //     return items;
    // }


    @Override
    public String getDescripcion() {
        return "Cogigo: "+ super.getCodigo()+ "Nombre: "+ super.getNombre()+ "Precio Unitario: "+ super.getPrecio();
    }

    @Override
    public BigDecimal getPrecioTotal() {
        return (super.getPrecio()).multiply(new BigDecimal(cantidad));
    }

    @Override
    public String getCodigo(){
        return super.getCodigo();
    }
    



}
