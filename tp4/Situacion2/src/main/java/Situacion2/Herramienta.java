package Situacion2;

import java.math.BigDecimal;

public abstract class Herramienta implements ItemVenta {
    private String codigo;
    private String nombre;
    //private String funcionalidad;
    private BigDecimal precio;

    // ---Constructor---
    public Herramienta(String codigo, String nombre, BigDecimal precio) {
        this.setCodigo(codigo);
        this.setNombre(nombre);
        this.setPrecio(precio);
    }
    //---getters y setters---

    public BigDecimal getPrecio() {
        return precio;
    }

    public void setPrecio(BigDecimal precio) {
        this.precio = precio;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    
    
}


