package Situacion2;

import java.math.BigDecimal;

public interface ItemVenta {

    
    BigDecimal getPrecioTotal();
    String getCodigo();
    String getDescripcion();
}
