package Situacion2;

import java.math.BigDecimal;

public class Manual extends Herramienta {
    private BigDecimal precio;
    private Integer cantidad;

    public Manual(String codigo, String nombre, BigDecimal precio, Integer cantidad) {
        super(codigo, nombre, precio);
        this.cantidad= cantidad;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    @Override
    public BigDecimal getPrecioTotal() {
        return precio.multiply(new BigDecimal(cantidad));
    }
    @Override
    public String getDescripcion() {
        return "Codigo: "+ super.getCodigo() +" , nombre: "+ super.getNombre() + ", precio: " + super.getPrecio();
    }
    @Override
    public String getCodigo(){
        return super.getCodigo();
    }
}
