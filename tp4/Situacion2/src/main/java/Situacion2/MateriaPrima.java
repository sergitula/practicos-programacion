package Situacion2;

import java.math.BigDecimal;

public class MateriaPrima extends Material {
    private Integer cantidadMateriaPrima;

    public MateriaPrima(String codigo, String nombre, BigDecimal precio, Integer cantidadMateriaPrima) {
        super(codigo, nombre, precio);
        this.setCantidadMateriaPrima(cantidadMateriaPrima);
    }

    public Integer getCantidadMateriaPrima() {
        return cantidadMateriaPrima;
    }

    public void setCantidadMateriaPrima(Integer cantidadMateriaPrima) {
        this.cantidadMateriaPrima = cantidadMateriaPrima;
    }

    @Override
    public BigDecimal getPrecioTotal() {
        return super.getPrecio();
    }


}
