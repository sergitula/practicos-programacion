package Situacion2;

import java.math.BigDecimal;

public abstract class Material {
    private String codigo;
    private String nombre;
    private BigDecimal precio;

    public abstract BigDecimal getPrecioTotal();

 
    // ---Constructor---
    public Material(String codigo, String nombre, BigDecimal precio) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.precio = precio;
    }

    //---Getters y Setters---
    public String getCodigo() {
        return codigo;
    }
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public BigDecimal getPrecio(){
        return precio;
    }
    public void setPrecio(BigDecimal precio) {
        this.precio = precio;
    }

    

    
}
