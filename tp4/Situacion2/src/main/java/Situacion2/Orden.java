package Situacion2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Orden {
    private String nombreEmpresa;
    List<ItemVenta> orden;

    // constructor 
    public Orden (String nombreEmpresa){
        this.nombreEmpresa = nombreEmpresa;
        orden = new ArrayList<ItemVenta>();
    }
    
    // setter y getter
    public String getNombreEmpresa() {
        return nombreEmpresa;
    }

    public void setNombreEmpresa(String nombreEmpresa) {
        this.nombreEmpresa = nombreEmpresa;
    }

    public void agregarItemVenta(ItemVenta item) {
        orden.add(item);
    }


    public int cantidadDeItems(){
        return orden.size();
    }

    public List<ItemVenta> mostrarItemsVenta() {
        return orden;
    }

    public void mostrarOrdenadosPorDescripcion(){
      //  Collections.sort(orden); // Falta
      mostrarOrden();
    }

    public void mostrarOrdenadorPorPrecio(){
        // Collections.sort(orden); //Falta
        mostrarOrden();
    }

    public void mostrarOrden(){
        for(ItemVenta item : orden){
            System.out.println(item);
        }
    }

}
