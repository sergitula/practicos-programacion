package Situacion2;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;

public class OrdenTest {
    Orden orden;

    @Before
    public void init(){
        orden = new Orden("EmpresaLegal");
    }

    @Test
    public void testComprobarOrdenVacia(){
        // Orden orden = new Orden("EmpresaLegal");
        assertEquals(0, orden.cantidadDeItems());
    }


    @Test
    public void testAgregarItemsAOrden(){
        BigDecimal precio = new BigDecimal (700);
        Manual item = new Manual("GHJW34", "Martillo", precio, 2);
        BigDecimal precio2 = new BigDecimal(7800);
        BigDecimal consumo = new BigDecimal (123);
        Electrico item2 = new Electrico("HJE4D", "Taladro", precio2, consumo);
        orden.agregarItemVenta(item);
        orden.agregarItemVenta(item2);
        assertEquals(2, orden.cantidadDeItems());
    }
}
